
## Brainstorming
Done by every member of the team

We started the brainstorming process during a class. First, we thought the process was only about smart features, which is reflected by the first ideas thrown up. After that, Mario explained us that it was about every kind of idea, thus allowing us to propose even more. **Every member of the team participated in this stage.**

**Ideas thrown up:**

* The app analyzes at which time the passenger leaves and throws a recomendation at that hour.
    * This gives the user a comfort and care impression which helps fidelize them.
* If a driver or passenger cancels, the app will automatically search replacement.
    * This mitigates the problem of not having the car full in the case of the driver, and the problem of being able to go in time for the passengers.
* When the driver has a place in the car, the app recommends one user and displays which is the stimated extra time.
    * This one helps the driver stimate the consequence, for him/her and the passengers, of taking a new passenger. Thus allowing them to have a better service.
* Depending on the departure room, suggests a middle ground.
    * Again this gives the impression of care, minimizing the commute time for everyone.
* Searches automatically for default departure and arriving place.
    * Again this gives the impression of care, helping passengers remember to set apart their seat.
* (Non-smart) A driver can set up how many passengers will take.
    * Maybe a driver does not want to have their car full thanks to covid.
* Automatically pair up known drivers and passengers.
    * If there are drivers and passengers that go along well, suggesting them to travel together could help them have welfare.
* The app allows the driver to set up the route through points in the map.
    * That would allow passengers to see what is the driver's route to plan possible pick up times.


## Decision Process
Done by every member of the team

For the decision process with took the approach of the "Four Categories". We started off by assigning a category to each of the ideas that we had and then proceeded to discard and pick the best option for us. 

* **KUPI**: an idea for enabling human interactions within organizations, was set to be the rational choice, because it is fairly easy to implement on a technical level, the challenge would be to stablish a psychological framework to make relationships more dynamic. But we wanted a bigger challenge for the app.

* **The Medical Events** idea: for automizing healthcare events such as appointments and tests, and creating checklists for patients to follow medical plans more easily. This was set to be the long shot because of the implications that it would take to use medical information and unify communication amongst different entities. Also compliance with government regulations and changing the complete way of work of doctors and medical staff. 

* **Instagram Posts**: The darling idea was the optimization for posts in social media, because we all liked it very much and saw the potential for a great freemium-premium service that many would like to adopt in this era of influencers. Though, it involved the development of complex ML recommender systems.

* **Wheels**: This was the most likely to delight, and the one we set out to do. The idea of automating the process of Wheels, unifying a system for the whole university without managing +1000 users in the same group chat were great ideas for solving problems that we all felt identified with. It involves a challenge but we all think it is a manageable one and it would have great impact because we know many that complain about the current process and how much better it could be.


## Empathy Maps
Done by every member of the team

(1) First passenger empathy map:

![](Multimedia/Images/Empathy_Map_Santiago.jpeg)

(2) First Non wheels user:

![](Multimedia/Images/EmpathyMapNoWheels.png)

(3) Second Non wheels user:

![](Multimedia/Images/EmpathyMap_JuanFelipe.png)

(4) First driver empathy map:

![](Multimedia/Images/EmpathyMapDriver.png)

## Personas
Done by every member of the team

 Let the record show that, although individual students upload these images, **every member of the team participated at this stage.**

(1) First Driver Persona:

![](Multimedia/Images/DriverPersona.png)

(2) First Rider Persona:

![](Multimedia/Images/RiderPersona.png)

(3) First Non Wheels user Persona:

![](Multimedia/Images/NonWheels.png)

## General Description
Done by every member of the team

Wheels is a current solution for transportation in cars in Uniandes, however, it has tons of problems that people complain about. This solution works as a carpooling idea. 

Carpooling consists in sharing your ride as a driver to get to a desired destiny with other members of the community. You can charge for your ride and get encouragement to use the service again. For a raider, you get a comfortable and secure transportation. 

In uniandes, wheels work with WhatsApp groups. While it can do the job, there are big problems with it: 

    The tedious search for drivers or raiders. 

    The under commitment from all the users involved. 

    Not good defined routes. 

    Spam in WA groups. 

    Not enough space for members in WA groups. 

    Limited ways to pay. 

We, as a software development team, are going to develop a mobile app that tackles these problems and makes Wheels a 10 out of 10 service for all the Uniandes community. To make this possible, we tend to solve the above with the following features (below each problem): 

(1) The tedious search for drivers or raiders. 

The idea here is to make an automatization match between the drivers and raiders depending on the destiny. The Driver will be able to choose a route and the app will match it with the raiders that can benefit from the ride. 

(2) The under commitment from all users involved. 

There will be a feature that enables raiders to track drivers.  

(3) Not good defined routes. 

Before creating a ride, a driver can decide which route to take (we have different ideas on how a rider can select the route). 

(4) Spam in WA groups. 

The app won't need groups. It will connect the driver and the raider. The driver will also have a panel that updates raiders of the status of the ride. 

(5) Not enough space for members in WA groups. 

Same as point 4. 

(6) Limited ways to pay. 

The app will have a payment system. Automatic or not, depending on what the users want. 
